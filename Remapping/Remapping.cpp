#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}


void update_map(int &ind, Mat &map_x, Mat &map_y)
{
	for (int i = 0; i < map_x.rows; i++)
	{
		for (int j = 0; j < map_x.cols; j++)
		{
			switch (ind)
			{
			case 0:
				if (j > map_x.cols*0.25 && j < map_x.cols*0.75 && i > map_x.rows*0.25 && i < map_x.rows*0.75)
				{
					map_x.at<float>(i, j) = 2 * (j - map_x.cols*0.25f) + 0.5f;
					map_y.at<float>(i, j) = 2 * (i - map_x.rows*0.25f) + 0.5f;
				}
				else
				{
					map_x.at<float>(i, j) = 0;
					map_y.at<float>(i, j) = 0;
				}
				break;
			case 1:
				map_x.at<float>(i, j) = (float)j;
				map_y.at<float>(i, j) = (float)(map_x.rows - i);
				break;
			case 2:
				map_x.at<float>(i, j) = (float)(map_x.cols - j);
				map_y.at<float>(i, j) = (float)i;
				break;
			case 3:
				map_x.at<float>(i, j) = (float)(map_x.cols - j);
				map_y.at<float>(i, j) = (float)(map_x.rows - i);
				break;
			default:
				break;
			} // end of switch
		}
	}
	//ind = (ind + 1) % 4;
}

int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0, thresh;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	int ind = atoi(argv[3]);
	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;
	Mat src, imageDst;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		src = imread(pathI);
//		src = imread("6.png");
		numImg++;

		/*Do Something You want*/

		if (src.empty())
		{
			cout << "Could not open or find the image!\n" << endl;
			cout << "Usage: " << argv[0] << " <Input image>" << endl;
			return -1;
		}
		///////////

		Mat dst(src.size(), src.type());
		Mat map_x(src.size(), CV_32FC1);
		Mat map_y(src.size(), CV_32FC1);
		const char* remap_window = "Remap demo";
		namedWindow(remap_window, WINDOW_AUTOSIZE);
		//int ind = 0;
		//for (;;)
		//{
			update_map(ind, map_x, map_y);
			remap(src, dst, map_x, map_y, INTER_LINEAR, BORDER_CONSTANT, Scalar(0, 0, 0));
			imshow(remap_window, dst);
			/*char c = (char) waitKey(1000);
			if (c == 27)
			{
				break;
			}*/
		//}
		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/Remapping_" + strNumImg + ".png";
		bool iSuccess = imwrite(nameOp, dst);
		//cout << "nameOp: " << nameOp<<endl;
	}

	//waitKey(0);
	return 0;
}