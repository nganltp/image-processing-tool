/**
* @file filter2D_demo.cpp
* @brief Sample code that shows how to implement your own linear filters by using filter2D function
* @author OpenCV team
*/

#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
using namespace cv;
using namespace std;
/**
* @function main
*/
int main(int argc, char** argv)
{
	// Declare variables
	Mat src, dst;
	Mat kernel;
	Point anchor;
	double delta;
	int ddepth;
	int kernel_size;

	//![load]
	//const char* imageName = "6.png";

	// Loads an image
	string path = argv[1];
	src = imread(path); // Load an image

	if (src.empty())
	{
		cout<<" Error opening image\n";
		//printf(" Program Arguments: [image_name -- default ../data/lena.jpg] \n");
		//system("pause");
		return -1;
	}
	//![load]

	//![init_arguments]
	// initialize arguments for the filter
	anchor = Point(-1, -1);
	delta = 0;
	ddepth = -1;
	//![init_arguments]

	//// Loop - Will filter the image with different kernel sizes each 0.5 seconds
	int ind = 0;
	
//	namedWindow("src", WINDOW_AUTOSIZE);
	imshow("src", src);
	/*waitKey(0);*/
	
	
	
	
	for (;;)
	{
		//![update_kernel]
		// Update kernel size for a normalized box filter
		kernel_size = 3 + 2 * (ind % 15);
		kernel = Mat::ones(kernel_size, kernel_size, CV_32F) / (float)(kernel_size*kernel_size);
		//![update_kernel]

	//	![apply_filter]
		// Apply filter
		filter2D(src, dst, src.depth(), kernel, anchor/*, delta, BORDER_DEFAULT*/);
		//![apply_filter]
		namedWindow("dst", WINDOW_AUTOSIZE);
		imshow("dst", dst);
		//waitKey(0);
		char c = (char)waitKey(500);
		// Press 'ESC' to exit the program
		if (c == 27)
		{
			break;
		}

		ind++;
	}

	return 0;
}