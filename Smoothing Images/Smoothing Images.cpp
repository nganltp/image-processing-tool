#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}

int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int type, isize, numImg = 0;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	type = atoi(argv[3]);
	isize = atoi(argv[4]);

	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat img, dst;
	
	for (auto & p : fs::directory_iterator(pathIn)) 
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		img = imread(pathI);
		numImg++;

		/*blur*/
		if (type == 0)
		{
			blur(img, dst, Size(isize, isize));
		}

		/*Gaussian blur*/
		if (type == 1)
		{
			GaussianBlur(img, dst, Size(isize, isize), 0, 0);
		}

		/*Median blur*/
		if (type == 2)
		{
			medianBlur(img, dst, isize);
		}

		/*Bilateral Filtering*/
		if (type == 3)
		{
			bilateralFilter(img, dst, isize, isize * 2, isize / 2);
		}
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/SmoothOut_" + strNumImg + ".png";
		//cout << "nameOp: " << nameOp<<endl;
		bool iSuccess = imwrite(nameOp, dst);
	}
	waitKey(0);
	return 0;
}