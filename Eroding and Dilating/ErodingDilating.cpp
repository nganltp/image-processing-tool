#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}

Mat Erosion(int erosion_elem, int erosion_size,  Mat src)
{
	int erosion_type = 0;
	Mat erosion_dst;
	//int erosion_size = 5;
	if (erosion_elem == 0) { erosion_type = MORPH_RECT; }
	else if (erosion_elem == 1) { erosion_type = MORPH_CROSS; }
	else if (erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }
	Mat element = getStructuringElement(erosion_type,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));
	erode(src, erosion_dst, element);
	//imshow("Erosion Demo", erosion_dst);
	return erosion_dst;
}
Mat Dilation(int dilation_elem, int dilation_size, Mat src)
{
	int dilation_type = 0;
	Mat dilation_dst;
	//int dilation_size = 5;
	if (dilation_elem == 0) { dilation_type = MORPH_RECT; }
	else if (dilation_elem == 1) { dilation_type = MORPH_CROSS; }
	else if (dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }
	Mat element = getStructuringElement(dilation_type,
		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
		Point(dilation_size, dilation_size));
	dilate(src, dilation_dst, element);
	//imshow("Dilation Demo", dilation_dst);
	return dilation_dst;
}

int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0;
	int type,kernel_size, kernel_type;
	//double angle, scale;
	//int lowH, lowS, lowV, highH, highS, highV;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	type = atoi( argv[3]);
	kernel_size = atoi(argv[4]);
	kernel_type = atoi(argv[5]);
	
	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat src, imageDst;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		src = imread(pathI);
		numImg++;

		/*Do Something You want*/
		
		if (src.empty())
		{
			cout << "Could not open or find the image!\n" << endl;
			cout << "Usage: " << argv[0] << " <Input image>" << endl;
			return -1;
		}
		/*namedWindow("Erosion Demo", WINDOW_AUTOSIZE);
		namedWindow("Dilation Demo", WINDOW_AUTOSIZE);
		moveWindow("Dilation Demo", src.cols, 0);
		*/
		Mat dst;
		if (type == 0)
		dst = Erosion(kernel_type, kernel_size,src);
		else dst = Dilation(kernel_type, kernel_size,src);
		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/Ero_Dil_" + strNumImg + ".png";
		//cout << "nameOp: " << nameOp<<endl;
		bool iSuccess = imwrite(nameOp, dst);
	}
	/*imshow("Source image", src);
	imshow("Warp", warp_dst);
	imshow("Warp + Rotate", warp_rotate_dst);/*
	imshow("imageSrc", img);
	imshow("imageDst", imageDst);*/
	waitKey(0);
	return 0;
}