#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}

int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0;
	//double angle, scale;
	//int lowH, lowS, lowV, highH, highS, highV;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	/*angle = atof(argv[3]);
	scale = atof(argv[4]);
	*/
	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat src, imageDst;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		src = imread(pathI);
		numImg++;

		/*Do Something You want*/
		//CommandLineParser parser(argc, argv, path);
		//Mat src = imread(path);
		if (src.empty())
		{
			cout << "Could not open or find the image!\n" << endl;
			cout << "Usage: " << argv[0] << " <Input image>" << endl;
			return -1;
		}
		Point2f srcTri[3];
		srcTri[0] = Point2f(0.f, 0.f);
		srcTri[1] = Point2f(src.cols - 1.f, 0.f);
		srcTri[2] = Point2f(0.f, src.rows - 1.f);
		Point2f dstTri[3];
		dstTri[0] = Point2f(0.f, src.rows*0.33f);
		dstTri[1] = Point2f(src.cols*0.85f, src.rows*0.25f);
		dstTri[2] = Point2f(src.cols*0.15f, src.rows*0.7f);
		Mat warp_mat = getAffineTransform(srcTri, dstTri);
		Mat warp_dst = Mat::zeros(src.rows, src.cols, src.type());
		warpAffine(src, warp_dst, warp_mat, warp_dst.size());
		Point center = Point(warp_dst.cols / 2, warp_dst.rows / 2);
		double angle = -50.0;
		double scale = 0.6;
		Mat rot_mat = getRotationMatrix2D(center, angle, scale);
		/*Mat warp_rotate_dst;
		warpAffine(warp_dst, warp_rotate_dst, rot_mat, warp_dst.size());*/

		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/Shearing" + strNumImg + ".png";
		//cout << "nameOp: " << nameOp<<endl;
		bool iSuccess = imwrite(nameOp, rot_mat);
	}
	/*imshow("Source image", src);
	imshow("Warp", warp_dst);
	imshow("Warp + Rotate", warp_rotate_dst);/*
											 imshow("imageSrc", img);
											 imshow("imageDst", imageDst);*/
	waitKey(0);
	return 0;
}