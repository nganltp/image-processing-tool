#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;
//const char* window_name = "Edge Map";
/*
Mat src, src_gray;
Mat dst, detected_edges;
int lowThreshold = 0;
const int max_lowThreshold = 100;
const int kernel_size = 3;
const char* window_name = "Edge Map";
*/
Mat CannyThreshold(Mat src_gray, int lowThreshold, int kernel_size,Mat dst,Mat src)
{
	Mat detected_edges;
	blur(src_gray, detected_edges, Size(3, 3));
	int ratio = 3;
	Canny(detected_edges, detected_edges, lowThreshold, lowThreshold*ratio, kernel_size);
	dst = Scalar::all(0);
	src.copyTo(dst, detected_edges);
	return dst;
	//imshow(window_name, dst);
}

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}

int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0, lowThreshold, kernel_size;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	lowThreshold = atoi(argv[3]);
	kernel_size = 3;

	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat src,dst ,imageDst;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		src = imread(pathI);
		numImg++;

		/*Do Something You want*/
		if (src.empty())
		{
			std::cout << "Could not open or find the image!\n" <<endl;
			std::cout << "Usage: " << argv[0] << " <Input image>" << endl;
			return -1;
		}
		Mat src_gray;
		dst.create(src.size(), src.type());
		cvtColor(src, src_gray, COLOR_BGR2GRAY);
		//namedWindow(window_name, WINDOW_AUTOSIZE);
		//createTrackbar("Min Threshold:", window_name, &lowThreshold, max_lowThreshold, CannyThreshold);
		dst = CannyThreshold(src_gray, lowThreshold, kernel_size,dst,src);

		
		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/CannyEdge_" + strNumImg + ".png";
		//cout << "nameOp: " << nameOp<<endl;
		bool iSuccess = imwrite(nameOp, dst);
	}
	waitKey(0);
	return 0;
}