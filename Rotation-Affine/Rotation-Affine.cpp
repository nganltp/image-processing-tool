﻿#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}

int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0;
	double angle, scale;
	int lowH, lowS, lowV, highH, highS, highV;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	angle = atof(argv[3]);
	scale = atof(argv[4]);

	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat img, imageDst;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		img = imread(pathI);
		numImg++;

		/*Do Something You want*/
		imageDst = img.clone(); // Sao chép hai ma trận.

								//double angle = 45.0;
								//double scale = 1.5;
		Point2f center(img.cols / 2, img.rows / 2);

		Mat matRot = getRotationMatrix2D(center, angle, scale);
		warpAffine(img, imageDst, matRot, img.size());

		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/Rotation" + strNumImg + ".png";
		//cout << "nameOp: " << nameOp<<endl;
		bool iSuccess = imwrite(nameOp, imageDst);
	}
	imshow("imageSrc", img);
	imshow("imageDst", imageDst);
	waitKey(0);
	return 0;
}