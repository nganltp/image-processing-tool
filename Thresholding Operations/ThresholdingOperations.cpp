#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}

int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0;
	int lowH, lowS, lowV, highH, highS, highV;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	lowH = atoi(argv[3]);
	lowS = atoi(argv[4]);
	lowV = atoi(argv[5]);
	highH = atoi(argv[6]);
	highS = atoi(argv[7]);
	highV = atoi(argv[8]);

	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat img, img_HSV;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		img = imread(pathI);
		numImg++;

		 /*Do Something You want*/
		 // Convert from BGR to HSV colorspace
		cvtColor(img, img_HSV, COLOR_BGR2HSV);
		// Detect the object based on HSV Range Values
		inRange(img_HSV, Scalar(lowH, lowS, lowV), Scalar(highH, highS, highV), img_HSV);

		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/SmoothOut_" + strNumImg + ".png";
		//cout << "nameOp: " << nameOp<<endl;
		bool iSuccess = imwrite(nameOp, img_HSV);
	}
	waitKey(0);
	return 0;
}