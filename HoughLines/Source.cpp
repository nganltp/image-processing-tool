#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}



int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0, thresh;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	int type = atoi(argv[3]);
	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat src, imageDst;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		src = imread(pathI);
		numImg++;

		/*Do Something You want*/

		if (src.empty())
		{
			cout << "Could not open or find the image!\n" << endl;
			cout << "Usage: " << argv[0] << " <Input image>" << endl;
			return -1;
		}
		///////////

		///////
		Mat dst, src_gray;
		Mat cdst, cdstP;
		cvtColor(src, src_gray, COLOR_BGR2GRAY);
		Canny(src, dst, 50, 200, 3);
		// Copy edges to the images that will display the results in BGR
		cvtColor(dst, cdst, COLOR_GRAY2BGR);
		cdstP = cdst.clone();
		// Standard Hough Line Transform
		vector<Vec2f> lines; // will hold the results of the detection
		HoughLines(dst, lines, 1, CV_PI / 180, 150, 0, 0); // runs the actual detection
														   // Draw the lines
		for (size_t i = 0; i < lines.size(); i++)
		{
			float rho = lines[i][0], theta = lines[i][1];
			Point pt1, pt2;
			double a = cos(theta), b = sin(theta);
			double x0 = a * rho, y0 = b * rho;
			pt1.x = cvRound(x0 + 1000 * (-b));
			pt1.y = cvRound(y0 + 1000 * (a));
			pt2.x = cvRound(x0 - 1000 * (-b));
			pt2.y = cvRound(y0 - 1000 * (a));
			line(cdst, pt1, pt2, Scalar(0, 0, 255), 3, LINE_AA);
		}
		// Probabilistic Line Transform
		vector<Vec4i> linesP; // will hold the results of the detection
		HoughLinesP(dst, linesP, 1, CV_PI / 180, 50, 50, 10); // runs the actual detection
															  // Draw the lines
		for (size_t i = 0; i < linesP.size(); i++)
		{
			Vec4i l = linesP[i];
			line(cdstP, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, LINE_AA);
		}
		// Show results
		namedWindow("window_name", WINDOW_AUTOSIZE);
		imshow("Source", src);
		
		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/HoughLines_" + strNumImg + ".png";
		//cout << "nameOp: " << nameOp<<endl;
		
		if (type == 0)
		{
			bool iSuccess = imwrite(nameOp, cdst);
			imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst);
		}
			
		else
		{
			imwrite(nameOp, cdstP);
			imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP);
		}
	}

	waitKey(0);
	return 0;
}