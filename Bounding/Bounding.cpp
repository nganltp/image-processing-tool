#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}

Mat thresh_callback(Mat src_gray, int thresh)
{
	RNG rng(12345);
	Mat canny_output;
	Canny(src_gray, canny_output, thresh, thresh * 2);
	vector<vector<Point> > contours;
	findContours(canny_output, contours, RETR_TREE, CHAIN_APPROX_SIMPLE);
	vector<vector<Point> > contours_poly(contours.size());
	vector<Rect> boundRect(contours.size());
	vector<Point2f>centers(contours.size());
	vector<float>radius(contours.size());
	for (size_t i = 0; i < contours.size(); i++)
	{
		approxPolyDP(contours[i], contours_poly[i], 3, true);
		boundRect[i] = boundingRect(contours_poly[i]);
		minEnclosingCircle(contours_poly[i], centers[i], radius[i]);
	}
	Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
	for (size_t i = 0; i< contours.size(); i++)
	{
		Scalar color = Scalar(rng.uniform(0, 256), rng.uniform(0, 256), rng.uniform(0, 256));
		drawContours(drawing, contours_poly, (int)i, color);
		rectangle(drawing, boundRect[i].tl(), boundRect[i].br(), color, 2);
		circle(drawing, centers[i], (int)radius[i], color, 2);
	}
	return drawing;
}

int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0, thresh;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
	if (argc <= 3)
		thresh = 110;
	else
		thresh = atoi(argv[3]);

	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat src, imageDst;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		src = imread(pathI);
		numImg++;

		/*Do Something You want*/

		if (src.empty())
		{
			cout << "Could not open or find the image!\n" << endl;
			cout << "Usage: " << argv[0] << " <Input image>" << endl;
			return -1;
		}
		///////////

		///////
		Mat dst, src_gray;
		cvtColor(src, src_gray, COLOR_BGR2GRAY);
		blur(src_gray, src_gray, Size(3, 3));
		//const char* source_window = "Source";
		//namedWindow(source_window);
		//imshow(source_window, src);
		//const int max_thresh = 255;
//		createTrackbar("Canny thresh:", source_window, &thresh, max_thresh, thresh_callback);
		dst =  thresh_callback(src_gray, thresh);
		
		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/Bounding_" + strNumImg + ".png";
		//cout << "nameOp: " << nameOp<<endl;
		bool iSuccess = imwrite(nameOp, dst);
	}

	waitKey(0);
	return 0;
}