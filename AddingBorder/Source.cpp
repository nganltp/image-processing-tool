#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}



int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0, thresh;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
		int type = atoi(argv[3]);
	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat imageDst;
	Mat src, dst;
	int top, bottom, left, right;
	RNG rng(12345);

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		src = imread(pathI);
		numImg++;

		/*Do Something You want*/

		if (src.empty())
		{
			cout << "Could not open or find the image!\n" << endl;
			cout << "Usage: " << argv[0] << " <Input image>" << endl;
			return -1;
		}
		///////////

		top = (int)(0.05*src.rows); bottom = top;
		left = (int)(0.05*src.cols); right = left;
		int borderType;
			if(type == 0 )
			{
				borderType = BORDER_CONSTANT;
			}
			else if (type == 1)
			{
				borderType = BORDER_REPLICATE;
			}
			Scalar value(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			copyMakeBorder(src, dst, top, bottom, left, right, borderType, value);
		namedWindow("name");
		imshow("Adding bording", dst);
		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/AddBording_" + strNumImg + ".png";
		bool iSuccess = imwrite(nameOp, dst);
		//cout << "nameOp: " << nameOp<<endl;
	}

	waitKey(0);
	return 0;
}