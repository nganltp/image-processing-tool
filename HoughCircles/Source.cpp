#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <direct.h> //mkdir
#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>
#include <stdio.h>

using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

string Int2Str(int value)
{
	ostringstream os;
	os << value;
	return os.str();
}



int main(int argc, char **argv)
{
	string path, pathIn, pathOut;
	int numImg = 0, thresh;
	/*cmd*/
	pathIn = argv[1];
	pathOut = argv[2];
//	int type = atoi(argv[3]);
	path = "/image-processing-tool/";
	pathIn = path + pathIn;
	pathOut = path + pathOut;

	Mat src, imageDst;

	for (auto & p : fs::directory_iterator(pathIn))
	{
		ostringstream oss;
		oss << p;
		string pathI = oss.str();

		src = imread(pathI);
		numImg++;

		/*Do Something You want*/

		if (src.empty())
		{
			cout << "Could not open or find the image!\n" << endl;
			cout << "Usage: " << argv[0] << " <Input image>" << endl;
			return -1;
		}
		///////////

		Mat gray;
		cvtColor(src, gray, COLOR_BGR2GRAY);
		medianBlur(gray, gray, 5);
		vector<Vec3f> circles;
		HoughCircles(gray, circles, HOUGH_GRADIENT, 1,
			gray.rows,  // change this value to detect circles with different distances to each other
			100, 30, 50, 250 // change the last two parameters
						   // (min_radius & max_radius) to detect larger circles
		);
		Mat cdst = src.clone();
		for (size_t i = 0; i < circles.size(); i++)
		{
			Vec3i c = circles[i];
			Point center = Point(c[0], c[1]);
			// circle center
			circle(cdst, center, 1, Scalar(0, 100, 100), 3, LINE_AA);
			// circle outline
			int radius = c[2];
			circle(cdst, center, radius, Scalar(255, 0, 255), 3, LINE_AA);
		}
		namedWindow("name");
		imshow("detected circles", cdst);
		/*Write*/
		string strNumImg = Int2Str(numImg);
		string nameOp = pathOut + "/HoughCircles_" + strNumImg + ".png";
		bool iSuccess = imwrite(nameOp, cdst);
		//cout << "nameOp: " << nameOp<<endl;
	}

	waitKey(0);
	return 0;
}